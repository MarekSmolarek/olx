package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.android.RegistrationScreen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class RegistrationAndroidTest extends BaseTest {
    @BeforeClass
    public void startup(){

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context){
        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Zaczynam testy");
    }
    @DataProvider
    public Object[][] getRegister() {
        return new Object[][]{

                {"marek.te@spoko.pl", "123Test123"},
                {"marek.tes@spoko.pl", "123Test123"},
                {"marek.test@spoko.pl", "123Test123"}
        };
    }
    @DataProvider
    public Object[][] getNoEmail() {
        return new Object[][]{

                {" ", "123Test123"}
        };
    }

    @DataProvider
    public Object[][] getNoPassword() {
        return new Object[][]{

                {"marek.testowy11@spoko.pl", " "}
        };

    }
    @DataProvider
    public Object[][] getWrongEmail() {
        return new Object[][]{

                {"@spoko.pl", "123Test123"},
                {"marek.testowyspoko.pl", "123Test123"},
                {"marek.testowy11@spoko", "123Test123"}
        };

    }
    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{

                {"marek.testowy1111@spoko.pl", "1"},
                {"marek.testowy2222@spoko.pl", "a"},
                {"marek.testowy3333@spoko.pl", "!!!!"}
        };

    }

    @Test(dataProvider = "getRegister")
    public void testRegister(String email, String password){

        System.out.println("Test poprawnej rejestracji");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.registrationToApp(email, password);
        registrationScreen.goToHomeScreen();
    }


    @Test(dataProvider = "getNoEmail")
    public void testRegisterNoEmail(String email, String password){

        System.out.println("Test rejestracji bez emaila");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.registrationToApp(email, password);

    }

    @Test(dataProvider = "getNoPassword")
    public void testRegisterNoPassword(String email, String password){

        System.out.println("Test rejestracji bez hasła");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.registrationToApp(email, password);

    }
    @Test(dataProvider = "getWrongEmail")
    public void testRegisterWrongEmail(String email, String password) {

        System.out.println("Test rejestracji ze złym emailem");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.registrationToApp(email, password);
    }
    @Test(dataProvider = "getWrongPassword")
    public void testRegisterWrongPassword(String email, String password) {

        System.out.println("Test rejestracji ze złym hasłem");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.registrationToApp(email, password);
    }
    @AfterMethod
    public void tearDown(){
        System.out.println("Koniec testów.");
    }


}
