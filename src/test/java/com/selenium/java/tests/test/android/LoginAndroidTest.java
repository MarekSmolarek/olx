package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.android.LoginScreen;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class LoginAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {
        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Zaczynam testy");
    }

    @DataProvider
    public Object[][] getLog() {
        return new Object[][]{

                {"marek.testowy1@spoko.pl", "123Test123"},
                //  {"marek.testowy22@spoko.pl", "123Test123"},
        };
    }

    @DataProvider
    public Object[][] getNoEmail() {
        return new Object[][]{

                {" ", "123Test123"},
                // {"marek.testowy1@spoko.pl", "123Test123"}
        };
    }

    @DataProvider
    public Object[][] getNoPassword() {
        return new Object[][]{

                {"marek.testowy1@spoko.pl", " "},
                //   {"marek.testowy1@spoko.pl", "123Test123"}
        };
    }

    @DataProvider
    public Object[][] getFalseEmail() {
        return new Object[][]{

                {"marek.testowy222@spoko.pl", "123Test123"},
                //  {"marek.testowy1@spoko.pl", "123Test123"}

        };
    }

    @DataProvider
    public Object[][] getFalsePassword() {
        return new Object[][]{

                {"marek.testowy2@spoko.pl", "123Testfalse123"},
                // {"marek.testowy1@spoko.pl", "123Test123"}
        };
    }

    @Test(dataProvider = "getLog")
    public void testLog(String email, String password) {

        System.out.println("Test poprawnego logowania");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(email, password);
        System.out.println("Wpisano dane logowania?");
        try {
            driver.findElement(By.xpath("//*[contains(@text, 'Witaj! Oto nowy OLX!')]"));
        } catch (NoSuchElementException e) {
            Assert.fail("Failed");
        }
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'Witaj! Oto nowy OLX!')]")).isDisplayed());

        System.out.println("Testy wykonany pomyślnie, zalogowano na konto z adresem: " + email);
        loginScreen.loginScrollClose();

    }

    @Test
    public void testLogAborted() {
        System.out.println("Pomijanie logowania...");
        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginAborted();
        loginScreen.loginScrollClose();
        try {
            driver.findElement(By.xpath("//*[contains(@text, 'Znajdź coś dla siebie')]"));
        } catch (NoSuchElementException e) {
            Assert.fail("Failed assert");
        }
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'Znajdź coś dla siebie')]")).isDisplayed());
        System.out.println("Test wykonany pomyślnie ");
    }

    @Test(dataProvider = "getNoEmail")
    public void testLogNoEmail(String email, String password) {
        System.out.println("Test logowania bez wpisania emaila");
        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(email, password);
        System.out.println("Nie wpisano emaila?");
        try {
            driver.findElement(By.id("pl.tablica:id/btnLogInNew"));
        } catch (NoSuchElementException e) {
            Assert.fail("Failed assert");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnLogInNew")).isDisplayed());
        System.out.println("Test wykonany pomyślnie, nie zalogowano");
    }

    @Test(dataProvider = "getNoPassword")
    public void testLogNoPassword(String email, String password) {
        System.out.println("Test logowania bez wpisania hasla");
        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(email, password);
        System.out.println("Nie wpisano hasła?");
        try {
            driver.findElement(By.id("pl.tablica:id/btnLogInNew"));
        } catch (NoSuchElementException e) {
            Assert.fail("Failed assert");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnLogInNew")).isDisplayed());
        System.out.println("Test wykonany pomyślnie, nie zalogowano");
    }

    @Test(dataProvider = "getFalseEmail")
    public void testLogFalseEmail(String email, String password) {

        System.out.println("Test logowania z błędnym emailem");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(email, password);
        System.out.println("Wpisano błędny email?");
        try {
            driver.findElement(By.id("pl.tablica:id/btnLogInNew"));
        } catch (NoSuchElementException e) {
            Assert.fail("Failed assert");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnLogInNew")).isDisplayed());
        System.out.println("Test wykonany pomyślnie, nie zalogowano");
    }

    @Test(dataProvider = "getFalsePassword")
    public void testLogFalsePassword(String email, String password) {

        System.out.println("Test logowania z błędnym hasłem");
        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        System.out.println("Wpisano błędne hasło?");
        loginScreen.loginToApp(email, password);
        try {
            driver.findElement(By.id("pl.tablica:id/btnLogInNew"));
        } catch (NoSuchElementException e) {
            Assert.fail("Failed assert");
        }
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnLogInNew")).isDisplayed());
        System.out.println("Test wykonany pomyślnie, nie zalogowano");

    }

    @AfterMethod
    public void tearDown() {
        System.out.println("Koniec testów.");
    }

}