package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.AccountPage;
import com.selenium.java.pages.web.LoginPage;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
//@Epic("Regression Tests")
@Feature("Account actions tests")
public class AccountWebTest extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";
        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }


    @Test(description = "Test account actions")
    @Severity(SeverityLevel.MINOR)
    @Description("Test description: Actions in account")
    @Story("Actions in account")
    public void testAccount() {
        WebDriver driver = getDriver();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        AccountPage accountPage = PageFactory.initElements(driver, AccountPage.class);

        loginPage.loginToApp("marek.smolarek222@gmail.com", "testowanieappium");

        accountPage.accountMesseges();
        Assert.assertTrue(accountPage.btn_delivers.isDisplayed(), "Widać przycisk przesyłek ");
        System.out.println("Widać przycisk przsyłek, asercja udana");
        accountPage.accountPayments();
        accountPage.accountDelivers();
        accountPage.accountSettings();
        methodHelper.testScreenshot("testSaveChanges");
        accountPage.accountLogOut();
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        WebDriver driver = getDriver();
        System.out.println("Test ended");
        //  driver.close();
    }
}
