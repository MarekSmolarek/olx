package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.web.RegistrationPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class RegistrationWebTest extends BaseTest {
    @BeforeClass
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";
        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getRegister() {
        return new Object[][]{

                {"marek.testowy1234567@spoko.pl", "123Test123"},
                {"marek.testowy1@spoko.pl", "123Test123"}

        };
    }

    @DataProvider
    public Object[][] getNoEmail() {
        return new Object[][]{

                {" ", "123Test123"},
                {"marek.testowy12345678910@spoko.pl", "123Test123"}
        };
    }

    @DataProvider
    public Object[][] getNoPassword() {
        return new Object[][]{

                {"marek.testowy11@spoko.pl", " "},
                {"marek.testowy1123456789@spoko.pl", "123Test123"}
        };

    }

    @DataProvider
    public Object[][] getWrongEmail() {
        return new Object[][]{

                {"@spoko.pl", "123Test123"},
                {"marek.testowy1@spoko", "123Test123"}
        };

    }

    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{

                {"marek.testowy1@spoko.pl", "!!!!"},
                {"marek.testowy1@spoko.pl", "123Test123"}
        };

    }

    @Test(dataProvider = "getRegister")
    public void testTrueRegister(String email, String password) {

        System.out.println("Test poprawnej rejestracji");

        WebDriver driver = getDriver();
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.registrationToApp(email, password);
        Assert.assertFalse(registrationPage.btn_register.isDisplayed(), "Nie zarejestrowano, podano błędne dane");
        registrationPage.registrationBackToHome();
        System.out.println("Testy wykonany pomyślnie, zarejestrowano na konto z adresem: " + email);
    }

    @Test(dataProvider = "getNoEmail")
    public void testFalseRegisterNoEmail(String email, String password) {

        System.out.println("Test rejestracji bez emaila");
        WebDriver driver = getDriver();
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.registrationToApp(email, password);
        Assert.assertTrue(registrationPage.btn_register.isDisplayed(), "zajerestrowano a nie powinno w tym tescie");
        System.out.println("Test wykonany pomyślnie nie zarejestrowano");
    }

    @Test(dataProvider = "getNoPassword")
    public void testFalseRegisterNoPassword(String email, String password) {

        System.out.println("Test rejestracji bez hasła");

        WebDriver driver = getDriver();
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.registrationToApp(email, password);
        Assert.assertTrue(registrationPage.btn_register.isDisplayed(), "zajerestrowano a nie powinno w tym tescie");
        System.out.println("Test wykonany pomyślnie nie zarejestrowano");


    }

    @Test(dataProvider = "getWrongEmail")
    public void testFalseRegisterWrongEmail(String email, String password) {

        System.out.println("Test rejestracji ze złym emailem");

        WebDriver driver = getDriver();
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.registrationToApp(email, password);
        Assert.assertTrue(registrationPage.btn_register.isDisplayed(), "zajerestrowano a nie powinno w tym tescie");
        System.out.println("Test wykonany pomyślnie nie zarejestrowano");
    }

    @Test(dataProvider = "getWrongPassword")
    public void testFalseRegisterWrongPassword(String email, String password) {

        System.out.println("Test rejestracji ze złym hasłem");

        WebDriver driver = getDriver();
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.registrationToApp(email, password);
        Assert.assertTrue(registrationPage.btn_register.isDisplayed(), "zajerestrowano a nie powinno w tym tescie");
        System.out.println("Test wykonany pomyślnie nie zarejestrowano");
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("Koniec testów.");
        //  driver.close();
    }


}
