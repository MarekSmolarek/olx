package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.AddingAdPage;
import com.selenium.java.pages.web.LoginPage;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.awt.*;

@Listeners({TestListener.class})
//@Epic("Regression Tests")
@Feature("Adding Ad tests")
public class AddingAdWebTest extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";
        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getAdd() {
        return new Object[][]{

                {"marek.smolarek222@gmail.com", "testowanieappium", "Fajny rower ZOBACZ!", "Rowery górskie", "sell", "2000", true, true,
                        "Wspaniały rower kup go", "C:\\Users\\maras\\Desktop\\TESTER\\adding ad web photos\\1.png",
                        "C:\\Users\\maras\\Desktop\\TESTER\\adding ad web photos\\2.png",
                        "Bydgoszcz", "Kujawsko-pomorskie", "602500432"},
                {"marek.smolarek222@gmail.com", "testowanieappium", "Fajny basen ZOBACZ!", "Baseny", "exchange", " ", true, false,
                        "Wspaniały basen kup go", "C:\\Users\\maras\\Desktop\\TESTER\\adding ad web photos\\2.png",
                        "C:\\Users\\maras\\Desktop\\TESTER\\adding ad web photos\\3.png",
                        "Gdańsk", "Pomorskie", "510430567"},
                {"marek.smolarek222@gmail.com", "testowanieappium", "Fajna gra ZOBACZ!", "Gry", "give", " ", false, false,
                        "Wspaniała gra kup ją", "C:\\Users\\maras\\Desktop\\TESTER\\adding ad web photos\\3.png",
                        "C:\\Users\\maras\\Desktop\\TESTER\\adding ad web photos\\4.png",
                        "Poznań", "Wielkopolskie", "787124423"}
        };
    }

    @Test(dataProvider = "getAdd")
    @Severity(SeverityLevel.NORMAL)
    @Description("Test description: Adding Ad")
    @Story("Adding Ad with lot of parameters")
    public void testAddAd(String email, String password, String title, String cat, String dispose,
                          String price, Boolean used, Boolean priv, String descr, String path,String path2, String city,
                          String voivodeship, String phone
    ) throws AWTException {
        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        AddingAdPage addingAdPage = PageFactory.initElements(driver, AddingAdPage.class);
        loginPage.loginToApp(email, password);
        addingAdPage.addingAdFirst(title);
        addingAdPage.addingAdCat(cat);
        addingAdPage.addingAdSellChangeGive(dispose, price);
        addingAdPage.addingAdBNewOrUsed(used);
        addingAdPage.addingAdPrivOrCorporate(priv);
        addingAdPage.addingAdDescr(descr);
        addingAdPage.addingAdImage(path,path2);
        addingAdPage.addingAdLoc(city, voivodeship);
        addingAdPage.addingAdPhone(phone);
        addingAdPage.addingAdPreview();
        try {
            driver.findElement(By.xpath("//*[@id=\"innerLayout\"]/div[8]/div/p[2]/a[1]"));
        } catch (NoSuchElementException e) {
            Assert.fail("Test oblany, powinno dodać ogłoszenie");
        }
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"innerLayout\"]/div[8]/div/p[2]/a[1]")).isDisplayed());
        methodHelper.testScreenshot("testAfterAdding");
        System.out.println("Test wykonany pomyślnie, dodano ogłoszenie");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        WebDriver driver = getDriver();
        System.out.println("Test ended");
        driver.close();
    }
}

