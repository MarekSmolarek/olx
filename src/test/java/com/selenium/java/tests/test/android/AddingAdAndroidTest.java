package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.android.AddingAdScreen;
import com.selenium.java.pages.android.LoginScreen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class AddingAdAndroidTest extends BaseTest {
    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Zaczynam testy");
    }

    @DataProvider
    public Object[][] toAdd() {
        return new Object[][]{

                {"marek.smolarek222@gmail.com", "testowanieappium", 2, 3, 5, 7, "Fajny rower ZOBACZ!", "Sport i Hobby", "Rowery górskie",
                        "Rowery", true, true, "999", "sell", "Wspaniały rower kup go!!!",
                        "602581427"},
                {"marek.smolarek222@gmail.com", "testowanieappium", 8, 10, 12, 14, "Fajny tablet ZOBACZ", "Elektronika", "Tablety",
                        " ", true, false, " ", "exchange", "Wspaniały laptop kup go!!!",
                        "781427852"},
                {"marek.smolarek222@gmail.com", "testowanieappium", 14, 16, 18, 20, "Fajny basen ZOBACZ!", "Dom i Ogród", "Baseny",
                        "Ogród", false, false, " ", "give", "Wspaniały basen kup go!!!",
                        "608558621"}
        };

    }

    @Test(dataProvider = "toAdd")
    public void testAddingAd(String email, String password, int first, int second, int third, int fourth,
                             String title, String mainCat, String targetCat, String secondaryCat, boolean priv, boolean used,
                             String price, String dispose, String descr, String number) {

        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);

        loginScreen.loginToApp(email, password);
        loginScreen.loginScrollClose();
        addingAdScreen.adStartAdding();
        addingAdScreen.adAddingPhotos(first, second, third, fourth);
        addingAdScreen.adAddingTitle(title);
        addingAdScreen.adCategoryChoose(mainCat, targetCat, secondaryCat);
        addingAdScreen.adPrivAndUsed(priv, used);
        addingAdScreen.adSellChangeGive(dispose, price);
        addingAdScreen.adAddingDescription(descr);
        addingAdScreen.adSettingLocalization();
        addingAdScreen.adAddingPhone(number);
        addingAdScreen.adEndAdding();

    }

    @AfterMethod
    public void tearDown() {
        System.out.println("Koniec testów.");
    }

}

