package com.selenium.java.tests.test.android;

import com.selenium.java.help.MethodHelper;
import com.selenium.java.pages.android.AccountScreen;
import com.selenium.java.pages.android.LoginScreen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class AccountAndroidTest extends MethodHelper {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Zaczynam testy");
    }

    @Test
    public void testAccount() {
        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);

        loginScreen.loginToApp("marek.smolarek222@gmail.com", "testowanieappium");
        loginScreen.loginScrollClose();
        accountScreen.accNameChange("MarcoPolo2");
        accountScreen.accInfo();
        Assert.assertTrue(accountScreen.btn_addAd.getText().equals("Dodaj ogłoszeni"), "Błędne dane");
        accountScreen.accAdAdd();
        accountScreen.accPackages();
        accountScreen.accMesseges();
        accountScreen.accWallet();
        accountScreen.accSettings();
        accountScreen.accLogOut();
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("Koniec testów.");
    }

}
