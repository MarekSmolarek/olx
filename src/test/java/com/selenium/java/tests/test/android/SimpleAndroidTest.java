package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.android.LoginScreen;
import com.selenium.java.pages.android.RegistrationScreen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class SimpleAndroidTest extends BaseTest {
    @BeforeClass
    public void startup() {
        startAppiumServer();
    }

    @BeforeMethod

    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {
        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Start testów");
    }

    @Test
    public void testTest() {
    }

    @Test
    public void testReg() {

        WebDriver driver = getDriver();

        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        registrationScreen.registrationToApp("test.1f@gmail.com", "testjedenf");
    }

    @Test
    public void testLog() {

        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp("marek.smolarek222@gmail.com", "testowanieappium");
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("Koniec testów.");
    }
}