package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import com.selenium.java.pages.android.FiltrScreen;
import com.selenium.java.pages.android.LoginScreen;
import com.selenium.java.pages.android.SearchScreen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class SearchAndFiltrAndroidTest extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Zaczynam testy");
    }

    @DataProvider
    public Object[][] toFiltr() {
        return new Object[][]{

                {"marek.smolarek222@gmail.com", "testowanieappium", "Rower Górski", "bicycles", "choosenToU", "sell", "100", "10000", false, false,},
                {"marek.smolarek222@gmail.com", "testowanieappium", "Gry ps4", "games", "cheapest", "exchange", " ", " ", true, false,},
                {"marek.smolarek222@gmail.com", "testowanieappium", "Basen", "pools", "mExpensive", "give", " ", " ", true, true,},
        };
    }

    @Test(dataProvider = "toFiltr")
    public void testSearchAndFiltr(String email, String password, String search, String thing, String sort, String dispose, String
            minprice, String maxprice, boolean priv, boolean used) {

        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        SearchScreen searchScreen = PageFactory.initElements(driver, SearchScreen.class);
        FiltrScreen filtrScreen = PageFactory.initElements(driver, FiltrScreen.class);

        loginScreen.loginToApp(email, password);
        loginScreen.loginScrollClose();
        searchScreen.searchItems(search);
        searchScreen.searchDifferentItems(thing);
        searchScreen.searchTap();
        filtrScreen.filtrFiltringItemsFirst(sort);
        filtrScreen.filtrMinMaxDistance();
        filtrScreen.filtrSellChangeGive(dispose, minprice, maxprice);
        filtrScreen.filtrPrivAndUsed(priv, used);
        filtrScreen.filtrFiltringItemsLast();
        methodHelper.testScreenshot("test Screenshot after search and filtr");

    }

    @Test
    public void testSearchAndAddToFav() {
        WebDriver driver = getDriver();

        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        SearchScreen searchScreen = PageFactory.initElements(driver, SearchScreen.class);

        loginScreen.loginToApp("marek.smolarek222@gmail.com", "testowanieappium");
        loginScreen.loginScrollClose();
        searchScreen.searchBicycle("Rower górski");
        searchScreen.searchTap();
        searchScreen.searchAddToFav();
    }


    @AfterMethod
    public void tearDown() {
        System.out.println("Koniec testów.");
    }

}