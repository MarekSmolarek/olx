package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.LoginPage;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
@Listeners({TestListener.class})
//@Epic("Regression Tests")
@Feature("Login tests")
public class LoginWebTest extends BaseTest {

MethodHelper methodHelper = new MethodHelper();

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value={"Platform","Browser"})
    public void setUp(String platform, String browser, ITestContext context){

        headless = false;
        url="https://www.olx.pl";
        launchWeb(platform,browser,context);
        System.out.println("Test started");
    }
    @DataProvider
    public Object[][] getLog() {
        return new Object[][]{

                {"marek.testowy1@spoko.pl", "123Test123"},
                {"marek.testowy2243658@spoko.pl", "123Test123"},
        };
    }
    @DataProvider
    public Object[][] getNoEmail() {
        return new Object[][]{

                {" ", "123Test123"},
                {"marek.testowy1@spoko.pl", "123Test123"}
        };
    }

    @DataProvider
    public Object[][] getNoPassword() {
        return new Object[][]{

                {"marek.testowy1@spoko.pl", " "},
                {"marek.testowy1@spoko.pl", "123Test123"}
        };
    }

    @DataProvider
    public Object[][] getFalseEmail() {
        return new Object[][]{

                {"marek.testowy222687678@spoko.pl", "123Test123"},
                {"marek.testowy1@spoko.pl", "123Test123"}

        };
    }

    @DataProvider
    public Object[][] getFalsePassword() {
        return new Object[][]{

                {"marek.testowy2@spoko.pl", "123Testfalse123"},
                {"marek.testowy1@spoko.pl", "123Test123"}
        };
    }

    @Test(dataProvider = "getLog",description = "Login with true parameters")
    @Severity(SeverityLevel.MINOR)
    @Description("Test description: Login with true parameters")
    @Story("True logging test")
    public void testTrueLog(String email, String password){

        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(email,password);
        methodHelper.testScreenshot("testTruelog");
        Assert.assertFalse(loginPage.btn_log.isDisplayed(), "Nie zalogowano, a powinno");
        System.out.println("Test wykonany pomyślnie, zalogowano na konto z adresem: " + email);


    }
    @Test(dataProvider = "getNoEmail",description = "Login with false parameters")
    @Severity(SeverityLevel.NORMAL)
    @Description("Test description: Login with false parameters")
    @Story("Invalid logging with no Email test")
    public void testFalseLogNoEmail(String email, String password) {
        System.out.println("Test logowania bez wpisania emaila");
        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(email,password);
        methodHelper.testScreenshot("testFalseLogNoEmail");
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed(),"Test oblany, nie miało zalogować");
        System.out.println("Test wykonany pomyślnie, nie zalogowano");
    }

    @Test(dataProvider = "getNoPassword",description = "Login with false parameters")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Test description: Login with false parameters")
    @Story("Invalid logging with no Password test")
    public void testFalseLogNoPassword(String email, String password) {
        System.out.println("Test logowania bez wpisania hasla");
        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(email,password);
        methodHelper.testScreenshot("testFalseLogNoPassword");
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed(),"Test oblany, nie miało zalogować");
        System.out.println("Test wykonany pomyślnie, nie zalogowano");
    }

    @Test(dataProvider = "getFalseEmail",description = "Login with false parameters")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Test description: Login with false parameters")
    @Story("Invalid logging with false Password test")
    public void testFalseLogFalseEmail(String email, String password) {

        System.out.println("Test logowania z błędnym emailem");

        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(email,password);
        methodHelper.testScreenshot("testFalseLogFalseEmail");
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed(),"Test oblany, nie miało zalogować");
        System.out.println("Test wykonany pomyślnie, nie zalogowano");
    }

    @Test(dataProvider = "getFalsePassword",description = "Login with false parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("Test description: Login with false parameters")
    @Story("Invalid logging with false Email test")
    public void testFalseLogFalsePassword(String email, String password) {

        System.out.println("Test logowania z błędnym hasłem");
        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(email,password);
        methodHelper.testScreenshot("testFalseLogFalsePassword");
        try {
            driver.findElement(By.id("se_userLogin"));
        } catch (NoSuchElementException e) {
            Assert.fail("Zalogowano a nie powinno");
        }
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed(),"Test oblany, nie miało zalogować");
        System.out.println("Test wykonany pomyślnie, nie zalogowano");

    }


    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        WebDriver driver = getDriver();
        System.out.println("Test ended");
        driver.close();
    }
}
