package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.SearchAndFiltrPage;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
//@Epic("Regression Tests")
@Feature("Searching and Filtring tests")
public class SearchAndFiltrWebTest extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";
        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getSearch() {
        return new Object[][]{

                {"Rower górski", "Bydgoszcz", "bicycles", "100", "5000", true, false, "cheapest", "MTB FOCUS Whistler",
                        "Witam i o nic nie pytam"},
                {"Basen ogrodowy", "Poznań", "pools", "300", "7000", false, false, "mExpensive", "BASEN OGRODOWY STELAŻOWY 305x76cm",
                        "Witam i o nic nie pytam"},
                {"Gry ps4", "Gdańsk", "games", "200", "3000", true, true, "newest", "Gry PS4. Dark Souls 3,",
                        "Witam i o nic nie pytam"}
        };
    }


    @Test(dataProvider = "getSearch", description = "Test search and filtr")
    @Severity(SeverityLevel.MINOR)
    @Description("Test description: Search and filtr")
    @Story("Searching and filtring items")
    public void testSearchAndFiltr(String item, String loc, String cat, String pricemin, String pricemax, boolean used, boolean priv,
                                   String sort, String sItem, String mess) {

        WebDriver driver = getDriver();
        SearchAndFiltrPage searchAndFiltrPage = PageFactory.initElements(driver, SearchAndFiltrPage.class);
        searchAndFiltrPage.searchItems(item);
        searchAndFiltrPage.searchLoc(loc);
        searchAndFiltrPage.searchAtributes();
        searchAndFiltrPage.searchPrice(pricemin, pricemax);
        searchAndFiltrPage.searchBNewOrUsed(used);
        searchAndFiltrPage.searchPrivOrCorporate(priv);
        searchAndFiltrPage.searchSort(sort);
        searchAndFiltrPage.searchDifferentCat(cat);
        methodHelper.testScreenshot("testAfterFiltring");
        searchAndFiltrPage.searchActionsInAd(sItem);
        searchAndFiltrPage.searchSendMess(mess);
        methodHelper.testScreenshot("testAfterMess");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        WebDriver driver = getDriver();
        System.out.println("Test ended");
        driver.close();
    }
}
