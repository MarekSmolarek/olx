package com.selenium.java.tests.examples;

public class CodeExample2 {
    public static void main(String[] args) {
        Person marek = new Person();

        marek.name = "Marek";
        marek.age = 26;
        marek.isAlive = true;

        Person mikolaj = new Person();

        mikolaj.name = "Mikolaj";
        mikolaj.age = 999;
        mikolaj.isAlive = false;

        marek.przedstawSie();
        mikolaj.przedstawSie();

        Person michal = new Person();
        michal.przedstawSie("Michal", 18, true);
        Person andrzej = new Person();
        andrzej.przedstawSie("Andrzej", 88, false);
    }
}
