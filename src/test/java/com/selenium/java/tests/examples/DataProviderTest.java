package com.selenium.java.tests.examples;

import org.testng.annotations.*;

public class DataProviderTest{
    @BeforeClass
    public void startup(){
        System.out.println("Startup");
    }
    @BeforeMethod
    public void setUp(){
        System.out.println("Before Party");
    }
    @DataProvider
    public Object[][] getData(){
        return new Object[][]{
                {5,"five",6},
                {7,"seven",8},
                {2,"two",3},
                {1,"four",4},
        };
    }
    @Test(dataProvider = "getData")
    public void lobby(int p1, String p2, int p3){
        System.out.println("Test");
        System.out.println(p1 + " "+ p2 + " " + p3);
    }


    @AfterMethod
    public void tearDown(){
        System.out.println("After Party");
    }
    @AfterClass
    public void stop(){
        System.out.println("After Class");
    }
}