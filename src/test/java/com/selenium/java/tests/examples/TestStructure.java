package com.selenium.java.tests.examples;

import org.testng.annotations.*;

public class TestStructure {
    @BeforeClass
    public void startup(){
        System.out.println("Startup");
    }
    @BeforeMethod
    public void setUp(){
        System.out.println("Before Party");
    }
    @Test
    public void test1(){
        System.out.println("Test1");
    }
    @Test
    public void test2(){
        System.out.println("Test2");
    }
    @Test
    public void test3(){
        System.out.println("Test3");
    }
    @Test
    public void test4(){
        System.out.println("Test4");
    }
    @AfterMethod
    public void tearDown(){
        System.out.println("After Party");
    }
    @AfterClass
    public void stop(){
        System.out.println("After Class");
    }
}

