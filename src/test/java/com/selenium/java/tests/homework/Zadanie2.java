package com.selenium.java.tests.homework;
import java.util.Arrays;
public class Zadanie2 {
    public enum Imiona {
        MAREK,
        DAREK,
        CZAREK,
        AREK,
        MICHAL,
        ADAM,
        PAWEL,
        PIOTR,
        DAMIAN,
        TOMEK,
    }
    public static void main(String[] args) {
        String tablica[] = {
                imiona(Imiona.MAREK),
                imiona(Imiona.DAMIAN),
                imiona(Imiona.ADAM),
                imiona(Imiona.AREK),
                imiona(Imiona.CZAREK),
                imiona(Imiona.MICHAL),
                imiona(Imiona.PAWEL),
                imiona(Imiona.PIOTR),
                imiona(Imiona.TOMEK),
                imiona(Imiona.DAREK)};
        for (String item : tablica) {
            System.out.println(item);
        }
        System.out.println(tablica.length);

        Arrays.sort(tablica);
        for (String sortowanie : tablica) {
            System.out.println(sortowanie);
        }

    }

    public static String imiona(Imiona imionka) {
        String firstName;
        switch (imionka) {
            case ADAM:
                firstName = "Adam";
                break;
            case AREK:
                firstName = "Arek";
                break;
            case DAREK:
                firstName = "Darek";
                break;
            case MAREK:
                firstName = "Marek";
                break;
            case PAWEL:
                firstName = "Pawel";
                break;
            case PIOTR:
                firstName = "Piotr";
                break;
            case TOMEK:
                firstName = "Tomek";
                break;
            case CZAREK:
                firstName = "Czarek";
                break;
            case DAMIAN:
                firstName = "Damian";
                break;
            case MICHAL:
                firstName = "Michal";
                break;
            default:
                firstName = " ";
        }
        return firstName;
    }
}
