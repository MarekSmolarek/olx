package com.selenium.java.tests.homework;

public class Obiekty {
    public static void main(String[] args) {

        Srednia marek = new Srednia();      //obiekt marek i wykonuje parametry z klasy "Srednia", oceny podaje w tablicy)
        marek.srednia("Marek", "Waszak", new double[]{4, 3.5, 5, 2.5, 3.5, 4, 5, 6, 3});

        Srednia darek = new Srednia();
        darek.srednia("Darek", "Pyrek", new double[]{2, 2.5, 5, 6, 1, 3.5, 4.5, 2});

        Srednia piotr = new Srednia();
        piotr.srednia("Piotr", "Wąsik", new double[]{6, 6, 6, 4, 3.5, 3, 1, 1, 1.5, 5, 6});

    }

}

