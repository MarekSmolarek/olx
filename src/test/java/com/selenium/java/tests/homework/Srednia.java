package com.selenium.java.tests.homework;

public class Srednia {
    void srednia(String firstName, String secondName, double[] ocena) {                //metoda
        double dlugosc = ocena.length;                                                 //legnth ze wszystkie elementy z tablicy ocena
        double suma = 0;                                                               //inicjalizuje sumowanie od 0
        for (int i = 0; i < dlugosc; i++) {                                            //petla dla sumująca oceny w tablicy
            suma = suma + ocena[i];
        }
        double srednia = suma / dlugosc;                                               //funkcja obliczajaca srednia
        double srednia2 = (Math.round(srednia * 100) / 100.0);                         //zaokraglanie do 2msc po przecinku(najpierw mnoze x100 zaokraglam i dziele/100)
        System.out.println("Średnia ocena dla ucznia " + firstName + " " + secondName + " wynosi " + srednia2);
    }
}