package com.selenium.java.tests.presentation;

public class CompareStringsAndIfElse {
    public static void main(String[] args) {
//      compareStrings_Positive();
//      compareStrings_Positive2();
//      compareStrings_Negative();
//      compareStrings_Negative2();
        compareStrings();
    }
    //region if else
    public static void compareStrings_Positive() {

        String object1 = "test";
        String object2 = "test";
        if (!object1.equals(object2)) {
            System.out.println("Work as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings_Positive2() {
        String object1 = "test";
        String object2 = "test";
        if (object1 == object2) {
            System.out.println("Work as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings_Negative() {
        String object1 = "test";
        String object2 = "test_2";
        if (object1.equals(object2)) {
            System.out.println("Work as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings_Negative2() {
        String object1 = "test";
        String object2 = "test_2";
        if (object1 != object2) {
            System.out.println("Work as expected!");
        } else {
            System.out.println("Nope...");
        }
    }

    public static void compareStrings() {
        String object1 = "test";
        String object2 = "test2";
        String object3 = "test3";
        if (object1.equals(object2)) {
            System.out.println("obj1=obj2");

        } else if (object1.equals(object3)) {
            System.out.println("obj1=obj3");

        } else if (object2.equals(object3)) {
            System.out.println("obj2=obj3");
        } else {
            System.out.println("Nopeeeeeeee");
        }
    }
//endregion if else
}
