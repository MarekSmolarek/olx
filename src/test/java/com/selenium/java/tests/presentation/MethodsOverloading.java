package com.selenium.java.tests.presentation;

public class MethodsOverloading {
    public static void main(String[] args) {
        method("Ewa ");
        method("Karolina", 12);
        method();
    }

    public static void method(String name, int age) {
        System.out.println(name + " ma " + age + " lat");
    }

    public static void method(String name) {
        System.out.println(name + "ma 65 lat");
    }

    public static void method() {
        System.out.println("Ala ma 35 lat");
    }
}