package com.selenium.java.tests.presentation;

public class DataTypes {
    public static void main(String[] args) {
        byte daysInMonth = 31;
        short daysInYear = 365;
        int age = 18;
        long veryLargeNumber = 712234234342L;
        float pi = 3.14F;
        double g = 9.80654d;
        char questionMark = '?';
        boolean bo1 = true;
        boolean bo2 = false;

        String name = "Marek";
        String lastName = "Smolarek";

        System.out.println(name + " " + lastName);

        System.out.println(daysInMonth);
        System.out.println(daysInYear);
        System.out.println(age);
        System.out.println(veryLargeNumber);
        System.out.println(pi);
        System.out.println(g);
        System.out.println(questionMark);
        System.out.println(bo1);
        System.out.println(bo2);
    }
}
