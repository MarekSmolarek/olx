package com.selenium.java.tests.presentation;


public class Switch {
    private enum Colors{
        BLUE,
        RED,
        YELLOW,
        PINK,
        NONE
    }
    public static void main(String[]args){
        ChooseColor(Colors.RED);
        ChooseColor(Colors.BLUE);
        ChooseColor(Colors.NONE);
    }
    private static void ChooseColor(Colors colors){
            switch (colors){
                case RED:
                    System.out.println("yay! it's red");
                    break;
                case BLUE:
                    System.out.println("yay! it's blue");
                    break;
                case YELLOW:
                    System.out.println("yay! it's yellow");
                    break;
                case PINK:
                    System.out.println("yay! it's pink");
                    break;
                default:
                    System.out.println("none");
            }
        }
    }
