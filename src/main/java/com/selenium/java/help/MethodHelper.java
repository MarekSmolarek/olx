package com.selenium.java.help;

import com.selenium.java.base.BaseTest;
import com.selenium.java.listeners.TestListener;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class MethodHelper extends BaseTest {

    //region GetScreenshot
    public void getScreenShot(String text) {
        WebDriver driver = getDriver();
        File file = new File("imagesFromTests/");
        String imagePath = file.getAbsolutePath();
        File srcFile;
        File targetFile;
        String pathToNewFile = imagePath + "/" + text;
        srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        targetFile = new File(pathToNewFile);
        try {
            FileUtils.copyFile(srcFile, targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //endregion GetScreenshot

    //region longPress
    public void longPress(double point_x, double point_y) {
        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point((int) point_x, (int) point_y)).perform();
    }
    //endregion longPress

    //region BackBtn
    public void pressAndroidBackBtn() {
        WebDriver driver = BaseTest.getDriver();
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
    }
    //endregion BackBtn

    //region WaitTime
    public void waitTime(int x) {
        try {
            Thread.sleep(x * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    //endregion WaitTime

    //region TapCoordinates
    public void tapCoordinates(double point_x, double point_y) {
        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).tap(PointOption.point((int) point_x, (int) point_y)).perform();
    }
    //endregion TapCoordinates

    //region Swipe
    public boolean swipeToElementById(String id) {
        WebDriverWait wait = getWaitDriver(1);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public boolean swipeToElementByText(String text) {
        WebDriverWait wait = getWaitDriver(1);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@text, '" + text + "')]")));
            return false;
        } catch (Exception e) {
            return true;
        }
    }
    //endregion swipe

    //region swipeByCoordinates
    public void swipeByCordinates(double start_x, double start_y, double end_x, double end_y) {
        WebDriver driver = BaseTest.getDriver();
        new TouchAction((PerformsTouchActions) driver).press(PointOption.point((int) start_x, (int) start_y))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
                .moveTo(PointOption.point((int) end_x, (int) end_y)).release().perform();
    }
    //endregion swipeByCoordinates

    //region ScrollByID
    public void scrollId(String element, BaseTest.direction direct, String start, double power, int iteration) {
        int i = 0;
        while (swipeToElementById(element) && i < iteration) {
            swipeInDirection(direct, start, power);
            i++;
        }
    }
    //endregion scrollByID

    //regionScrollByText
    public void scrollText(String element, BaseTest.direction direct, String start, double power, int iteration) {
        int i = 0;
        while (swipeToElementByText(element) && i < iteration) {
            swipeInDirection(direct, start, power);
        }

    }
    //endregion ScrollByText

    //region dragAndDrop
    public void dragAndDrop(double start_x, double start_y, double end_x, double end_y) {
        WebDriver driver = getDriver();
        TouchAction action = new TouchAction((PerformsTouchActions) driver);
        action.longPress(PointOption.point((int) start_x, (int) start_y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
                .moveTo(PointOption.point((int) end_x, (int) end_y)).release().perform();
    }
    //endregion dragAndDrop

    //region clickCoordintates
    public void clickCoordinates(double point_x, double point_y) {
        WebDriver driver = getDriver();
        Actions action = new Actions(driver);
        Actions moveMauseToClick = action
                .moveByOffset((int) point_x, (int) point_y).click();
        moveMauseToClick.perform();
    }
//endregion clickCoordinates

    //region uploadPhoto

    public void uploadPhoto(String path) throws AWTException {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        StringSelection ss = new StringSelection(path);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

        Robot robot = new Robot();
        robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
        robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
        robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyPress(java.awt.event.KeyEvent.VK_V);
        robot.keyRelease(java.awt.event.KeyEvent.VK_V);
        robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
        robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);

    }

    //endregion uploadPhoto

    //region testScreenshot
    public void testScreenshot(String name) {
        WebDriver driver = getDriver();
        TestListener testListener = new TestListener();

        if (driver instanceof WebDriver) {
            System.out.println("Screenshot captured for test:"+name);
            testListener.saveScreenshotPNG(driver);
        }
        testListener.saveTextLog("Screenshot after test: " + name);
    }
    //endregion testScreenshot
}