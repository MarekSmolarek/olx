package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends BaseTest {

    @FindBy(how = How.ID, using = "my-account-link")
    public WebElement btn_myAccount;
    @FindBy(how = How.ID, using = "userEmail")
    public WebElement input_Email;
    @FindBy(how = How.ID, using = "userPass")
    public WebElement input_Password;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cookiesBar\"]/button[1]")
    public WebElement btn_pop;
    @FindBy(how = How.ID, using = "se_userLogin")
    public WebElement btn_log;

    MethodHelper methodHelper = new MethodHelper();

    @Step("Login Step with user Email {0} and password {1}, for method {method} step...")
    public void loginToApp(String email, String password) {
        btn_myAccount.click();
        input_Email.sendKeys(email);
        methodHelper.waitTime(1);
        input_Password.sendKeys(password);
        btn_pop.click();
        btn_log.click();
    }

}
