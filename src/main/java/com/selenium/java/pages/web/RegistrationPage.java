package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RegistrationPage extends BaseTest {
    MethodHelper methodHelper = new MethodHelper();
    @FindBy(how = How.ID, using = "my-account-link")
    public WebElement btn_myAccount;
    @FindBy(how = How.ID, using = "register_tab")
    public WebElement btn_registerTab;
    @FindBy(how = How.ID, using = "userEmailRegister")
    public WebElement input_Email;
    @FindBy(how = How.ID, using = "userPassRegister")
    public WebElement input_Password;
    @FindBy(how = How.XPATH, using = "//*[@id=\"registerForm\"]/div[2]/div[2]/i")
    public WebElement btn_eyePass;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cookiesBar\"]/button[1]")
    public WebElement btn_pop;
    @FindBy(how = How.XPATH, using = "//*[@id=\"registerForm\"]/div[4]/div/div/label[1]")
    public WebElement btn_checkbox;
    @FindBy(how = How.ID, using = "button_register")
    public WebElement btn_register;
    @FindBy(how = How.XPATH, using = " //*[@id=\"innerLayout\"]/section/div/div/ul/li[2]/a")
    public WebElement btn_backToHome;


    public void registrationToApp(String email, String password) {
        btn_myAccount.click();
        methodHelper.waitTime(1);
        btn_registerTab.click();
        methodHelper.waitTime(1);
        input_Email.sendKeys(email);
        methodHelper.waitTime(1);
        input_Password.sendKeys(password);
        btn_eyePass.click();
        methodHelper.waitTime(2);
        btn_pop.click();
        methodHelper.waitTime(1);
        btn_checkbox.click();
        methodHelper.waitTime(1);
        btn_register.click();
    }

    public void registrationBackToHome() {
        methodHelper.waitTime(2);
        btn_backToHome.click();
    }
}
