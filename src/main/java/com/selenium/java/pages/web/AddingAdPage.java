package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;

import com.selenium.java.help.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.awt.*;


public class AddingAdPage extends BaseTest {

    MethodHelper methodhelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[@id=\"listContainer\"]/div/div/div/a/span")
    public WebElement btn_add;
    @FindBy(how = How.ID, using = "add-title")
    public WebElement input_addTitle;
    @FindBy(how = How.XPATH, using = "//*[@id=\"targetrenderSelect1-0\"]/dt/a")
    public WebElement btn_catClick;
    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/label[2]")
    public WebElement btn_free;
    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/label[3]")
    public WebElement btn_exchange;
    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/div/div[1]/input")
    public WebElement input_price;
    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/div/div[2]/label[2]")
    public WebElement btn_nego;
    @FindBy(how = How.XPATH, using = "//span[@class='chips__text' and contains(text(), 'Używane')]")
    public WebElement btn_used;
    @FindBy(how = How.XPATH, using = "//span[@class='chips__text' and contains(text(), 'Nowe')]")
    public WebElement btn_bNew;
    @FindBy(how = How.XPATH, using = "//span[@class='chips__text' and contains(text(), 'Osoba prywatna')]")
    public WebElement btn_priv;
    @FindBy(how = How.XPATH, using = "//span[@class='chips__text' and contains(text(), 'Firma')]")
    public WebElement btn_corpo;
    @FindBy(how = How.ID, using = "add-description")
    public WebElement input_addDescr;
    @FindBy(how = How.XPATH, using = "//*[@id=\"add-file-1\"]/div/a")
    public WebElement btn_addPhoto;
    @FindBy(how = How.XPATH, using = "//*[@id=\"add-file-2\"]/div/a")
    public WebElement btn_addPhoto2;
    @FindBy(how = How.ID, using = "mapAddress")
    public WebElement input_city;
    @FindBy(how = How.ID, using = "add-phone")
    public WebElement input_phone;
    @FindBy(how = How.XPATH, using = "//*[@id=\"preview-link\"]/span")
    public WebElement btn_preview;

    @Step("Adding Ad Step with title {0}")
    public void addingAdFirst(String title) {
        System.out.println("Klikam w dodawanie ogłoszenia");
        btn_add.click();
        input_addTitle.sendKeys(title);
    }
    @Step("Adding Ad Step with category {0}")
    public void addingAdCat(String cat) {
        btn_catClick.click();
        methodhelper.waitTime(2);

        WebElement target_category = getDriver().findElement(By.xpath("//strong[@class='category-name block " +
                "lheight14 fnormal small' and contains(text(), '" + cat + "')]"));

        System.out.println("Wybrano kategorie");

        target_category.click();
        methodhelper.waitTime(2);

    }
    @Step("Adding Ad Step with dispose choose {0} and price{1}")
    public void addingAdSellChangeGive(String dispose, String price) {

        System.out.println("Wybieram czy chce sprzedać, zamienić czy oddać za darmo...");
        switch (dispose) {
            case "sell":
                input_price.sendKeys(price);
                System.out.println("Zaznaczam cenę do negocjacji...");
                btn_nego.click();
                break;
            case "exchange":
                btn_exchange.click();
                break;
            case "give":
                btn_free.click();
                break;
        }
    }
    @Step("Adding Ad Step with new or used {0}")
    public void addingAdBNewOrUsed(boolean used) {
        System.out.println("Wybieram używane lub nowe");
        if (used) {
            btn_used.click();
        } else {
            btn_bNew.click();
        }
    }
    @Step("Adding Ad Step with privete or corporate {0}")
    public void addingAdPrivOrCorporate(boolean priv) {
        System.out.println("Klikam w prywatne lub firmowe ogłoszenie");
        if (priv) {
            btn_priv.click();
        } else {
            btn_corpo.click();
        }
    }
    @Step("Adding Ad Step with description: {0}")
    public void addingAdDescr(String descr) {
        System.out.println("Dodaję opis ogłoszenia");
        input_addDescr.sendKeys(descr);
    }
    @Step("Adding Ad Step with photo {0} and photo {1}")
    public void addingAdImage(String path,String path2) throws AWTException {
        btn_addPhoto.click();
        methodhelper.uploadPhoto(path);
        methodhelper.waitTime(3);
        btn_addPhoto2.click();
        methodhelper.uploadPhoto(path2);
        methodhelper.waitTime(3);
    }
    @Step("Adding Ad Step with city {0} and voivodeship {1}")
    public void addingAdLoc(String city, String voivodeship) {
        input_city.sendKeys(city);
        methodhelper.waitTime(3);

        WebElement chvoivodeship = getDriver().findElement(By.xpath("//span[@class='color-9' and contains(text(),'" + voivodeship + "')]"));
        methodhelper.waitTime(1);
        chvoivodeship.click();
    }
    @Step("Adding Ad Step with phone {0}")
    public void addingAdPhone(String phone) {
        input_phone.sendKeys(phone);
    }

    @Step("Adding Ad Step with preview {0}")
    public void addingAdPreview() {
        btn_preview.click();
    }
}