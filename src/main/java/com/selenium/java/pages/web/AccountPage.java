package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AccountPage extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[@id=\"se_accountAnswers\"]/span/span")
    public WebElement btn_mess;
    @FindBy(how = How.ID, using = "ws_messagesSent")
    public WebElement btn_messSent;
    @FindBy(how = How.XPATH, using = "//*[@id=\"globalLinks\"]/li[3]/a")
    public WebElement btn_messArch;
    @FindBy(how = How.XPATH, using = "//*[@id=\"se_accountWallet\"]/span")
    public WebElement btn_payments;
    @FindBy(how = How.XPATH, using = "//*[@id=\"globalLinks\"]/li[2]/a")
    public WebElement btn_invoices;
    @FindBy(how = How.XPATH, using = "//*[@id=\"globalLinks\"]/li[3]/a")
    public WebElement btn_invoicesE;
    @FindBy(how = How.XPATH, using = "//*[@id=\"usertabs\"]/div[1]/div[2]/div/a/span")
    public WebElement btn_boost;
    @FindBy(how = How.XPATH, using = "/html/body/div[3]/section/div/div/form/div/ol/li[2]/div[2]/div[2]/span[2]/a")
    public WebElement btn_dissmiss;
    @FindBy(how = How.XPATH, using = "//*[@id=\"globalLinks\"]/li[4]/a")
    public WebElement btn_points;
    @FindBy(how = How.XPATH, using = "//*[@id=\"usertabs\"]/div[1]/div[2]/div/div/div[1]/i")
    public WebElement btn_paymentsInfo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"se_delivery\"]/span")
    public WebElement btn_delivers;
    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div[2]/div/div[2]/div/div[1]/div/button[2]")
    public WebElement btn_buying;
    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div[2]/div/div[1]/ul/li[4]/a")
    public WebElement btn_settings;
    @FindBy(how = How.XPATH, using = "//*[@id=\"notification_center\"]/a/span")
    public WebElement btn_notifications;
    @FindBy(how = How.XPATH, using = "//*[@id=\"formNotificationCenter\"]/ul/li[2]/div[2]/label[1]")
    public WebElement btn_discountOffEmail;
    @FindBy(how = How.XPATH, using = "//*[@id=\"formNotificationCenter\"]/ul/li[2]/div[1]/label[1]")
    public WebElement btn_discountOffPhone;
    @FindBy(how = How.ID, using = "saveButtonNotificationCenter")
    public WebElement btn_settingsSave;
    @FindBy(how = How.ID, using = "login-box-logout")
    public WebElement btn_logout;

    @Step("Account step with messeges")
    public void accountMesseges() {
        System.out.println("Wchodzę w wiadomości");
        btn_mess.click();
        btn_messSent.click();
        methodHelper.waitTime(1);
        btn_messArch.click();
    }
    @Step("Account step with payments")
    public void accountPayments() {
        System.out.println("Wchodzę w płatności");
        btn_payments.click();
        methodHelper.waitTime(1);
        btn_invoices.click();
        methodHelper.waitTime(1);
        btn_invoicesE.click();
        methodHelper.waitTime(1);
        btn_points.click();
        methodHelper.waitTime(1);
        btn_paymentsInfo.click();
        methodHelper.waitTime(3);
        btn_boost.click();
        methodHelper.waitTime(2);
        btn_dissmiss.click();
    }
    @Step("Account step with delivers")
    public void accountDelivers() {
        System.out.println("Wchodzę w przesyłki");
        methodHelper.waitTime(1);

        methodHelper.waitTime(1);
        btn_delivers.click();
        methodHelper.waitTime(1);
        btn_buying.click();
    }
    @Step("Account step with settings")
    public void accountSettings() {
        System.out.println("Wchodzę w ustawnienia konta");
        methodHelper.waitTime(1);
        btn_settings.click();
        methodHelper.waitTime(1);
        btn_notifications.click();
        methodHelper.waitTime(1);
        btn_discountOffEmail.click();
        methodHelper.waitTime(1);
        btn_discountOffPhone.click();
        methodHelper.waitTime(1);
        btn_settingsSave.click();
    }
    @Step("Account step with log out")
    public void accountLogOut() {
        System.out.println("Wylogowywuję się");
        WebDriver driver = getDriver();
        Actions action = new Actions(driver);
        methodHelper.waitTime(4);
        WebElement we = driver.findElement(By.id("my-account-link"));
        action.moveToElement(we).build().perform();
        methodHelper.waitTime(2);
        btn_logout.click();
    }

}
