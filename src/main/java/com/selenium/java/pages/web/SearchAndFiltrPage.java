package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import com.selenium.java.listeners.TestListener;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
//@Epic("Regression Tests")
@Feature("Login tests")
public class SearchAndFiltrPage extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[@id=\"cookiesBar\"]/button[1]")
    public WebElement btn_pop;
    @FindBy(how = How.ID, using = "headerSearch")
    public WebElement input_search;
    @FindBy(how = How.XPATH, using = "//*[@id=\"autosuggest-div\"]/ul/li[1]/a")
    public WebElement btn_searchSuggest;
    @FindBy(how = How.ID, using = "cityField")
    public WebElement input_loc;
    @FindBy(how = How.XPATH, using = "//*[@id=\"autosuggest-geo-ul\"]/li[1]/a")
    public WebElement btn_locSuggest;
    @FindBy(how = How.ID, using = "distanceSelect")
    public WebElement btn_distance;
    @FindBy(how = How.XPATH, using = "//*[@id=\"distanceSelect\"]/dd/ul/li[9]/a")
    public WebElement btn_distance100;
    @FindBy(how = How.XPATH, using = "//*[@id=\"paramsListOpt\"]/div/div[1]/label[1]")
    public WebElement btn_searchInDes;
    @FindBy(how = How.XPATH, using = "//*[@id=\"paramsListOpt\"]/div/div[2]/label[1]")
    public WebElement btn_onlyWithPhoto;
    @FindBy(how = How.XPATH, using = "//*[@id=\"saveSearchCriteriaTop\"]/span/span")
    public WebElement btn_observeSearch;
    @FindBy(how = How.XPATH, using = "//span[@class='3rd-category-choose-label header block' and contains(text(), 'Wszystkie')]")
    public WebElement btn_cat1;
    @FindBy(how = How.XPATH, using = "//a[@class='tdnone block c27 brbott-4 category-choose category_choose1651 search-choose' and contains(text(), 'Rowery górskie')]")
    public WebElement btn_catBicycles;
    @FindBy(xpath = "//a[@class='tdnone block c27 brbott-4 category-choose category_choose1630 search-choose' and contains(text(), 'Baseny')]")
    public WebElement btn_catPools;
    @FindBy(xpath = "//a[@class='tdnone block c27 brbott-4 category-choose category_choose1603 search-choose' and contains(text(), 'Gry')]")
    public WebElement btn_catGames;
    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[1]/a/span[1]")
    public WebElement btn_pricemin;
    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[2]/a/span[1]")
    public WebElement btn_pricemax;
    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[1]/label/input")
    public WebElement input_priceminType;
    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[2]/label/input")
    public WebElement input_pricemaxType;
    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/a/span[1]")
    public WebElement btn_state;
    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/ul/li[2]/label[2]/span")
    public WebElement btn_used;
    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/ul/li[3]/label[2]/span")
    public WebElement btn_bNew;
    @FindBy(how = How.XPATH, using = "//*[@id=\"tabs-container\"]/div/div[2]/ul/li[2]/a/span[1]")
    public WebElement btn_priv;
    @FindBy(how = How.XPATH, using = "//*[@id=\"tabs-container\"]/div/div[2]/ul/li[3]/a/span[1]/span")
    public WebElement btn_corpo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dt/a")
    public WebElement btn_sort;
    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dd/ul/li[1]/a")
    public WebElement btn_newest;
    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dd/ul/li[2]/a")
    public WebElement btn_cheapest;
    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dd/ul/li[3]/a")
    public WebElement btn_mExpensive;
    @FindBy(how = How.XPATH, using = "//*[@id=\"viewSelector\"]/li[3]/a")
    public WebElement btn_viewMos;
    @FindBy(how = How.ID, using = "fancybox-close")
    public WebElement btn_noThx;
    @FindBy(how = How.XPATH, using = "//*[@id=\"contact_methods\"]/li[2]/div/strong")
    public WebElement btn_userCall;
    @FindBy(how = How.XPATH, using = "//*[@id=\"descImage\"]/img")
    public WebElement btn_clickPhoto;
    @FindBy(how = How.XPATH, using = "//*[@id=\"bigImage\"]/div[2]")
    public WebElement btn_nextPhoto;
    @FindBy(how = How.XPATH, using = "//*[@id=\"overgalleryclose\"]")
    public WebElement btn_closePhotos;
    @FindBy(how = How.XPATH, using = "//*[@id=\"contact_methods\"]/li[1]/a/span")
    public WebElement btn_sendMess;
    @FindBy(how = How.ID, using = "ask-text")
    public WebElement input_mess;

    @Step("Searching step with typing item {0}")
    public void searchItems(String item) {
        btn_pop.click();
        input_search.sendKeys(item);
        btn_searchSuggest.click();
    }

    @Step("Searching step with choosing loc {0}")
    public void searchLoc(String loc) {
        input_loc.sendKeys(loc);
        methodHelper.waitTime(4);
        btn_locSuggest.click();
        methodHelper.waitTime(2);
        btn_distance.click();
        methodHelper.waitTime(3);
        btn_distance100.click();
    }

    @Step("Searching step with choosing atributes")
    public void searchAtributes() {
        btn_searchInDes.click();
        btn_onlyWithPhoto.click();
        methodHelper.waitTime(3);
    }

    @Step("Searching step with choosing category {0}")
    public void searchDifferentCat(String cat) {
        System.out.println("Klikam kategorie");
        btn_cat1.click();
        methodHelper.waitTime(5);
        switch (cat) {
            case "bicycles":
                btn_catBicycles.click();
                break;
            case "pools":
                btn_catPools.click();
                break;
            case "games":
                btn_catGames.click();
                break;
        }
        methodHelper.waitTime(5);
    }

    @Step("Searching step with typing price min {0} and max {1) ")
    public void searchPrice(String pricemin, String pricemax) {
        btn_pricemin.click();
        methodHelper.waitTime(4);
        input_priceminType.sendKeys(pricemin);
        methodHelper.waitTime(4);
        btn_pricemax.click();
        input_pricemaxType.sendKeys(pricemax);
        btn_state.click();
    }

    @Step("Searching step with typing if ad is new or used {0}")
    public void searchBNewOrUsed(boolean used) {
        System.out.println("Klikam używane lub nowe");
        if (used) {
            btn_used.click();
        } else {
            btn_bNew.click();
        }
    }

    @Step("Searching step with typing if ad is private or corporate {0}")
    public void searchPrivOrCorporate(boolean priv) {
        System.out.println("Klikam w prywatne lub firmowe ogłoszenia");
        if (priv) {
            btn_priv.click();
        } else {
            btn_corpo.click();
        }
        methodHelper.waitTime(1);
    }

    @Step("Searching step with sorting ad")
    public void searchSort(String sort) {
        System.out.println("Zaznaczam sortowanie ...");
        methodHelper.waitTime(2);
        btn_sort.click();
        methodHelper.waitTime(1);
        switch (sort) {
            case "newest":
                btn_newest.click();
                break;
            case "cheapest":
                btn_cheapest.click();
                break;
            case "mExpensive":
                btn_mExpensive.click();
                break;
        }
        methodHelper.waitTime(2);
        btn_viewMos.click();
        methodHelper.waitTime(1);
        btn_observeSearch.click();
        methodHelper.waitTime(2);
        btn_noThx.click();
        methodHelper.waitTime(3);
    }

    @Step("Searching step with actions in ad (choosing exact ad {0}")
    public void searchActionsInAd(String sItem) {
        WebElement chItem = getDriver().findElement(By.xpath("//strong[contains(text(),'" + sItem + "')]"));
        chItem.click();
        methodHelper.waitTime(2);
        btn_clickPhoto.click();
        methodHelper.waitTime(6);
        btn_nextPhoto.click();
        methodHelper.waitTime(2);
        btn_closePhotos.click();
        methodHelper.waitTime(2);
        btn_userCall.click();
        methodHelper.waitTime(2);
    }

    @Step("Searching step with sending mess {0}")
    public void searchSendMess(String mess) {
        btn_sendMess.click();
        methodHelper.waitTime(2);
        input_mess.sendKeys(mess);
        methodHelper.waitTime(3);
        WebDriver driver = getDriver();
        driver.navigate().back();
    }

}