package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AccountScreen extends BaseTest {
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Konto')]")
    public WebElement btn_account;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'EDYTUJ')]")
    public WebElement btn_edit;
    @FindBy(how = How.ID, using = "pl.tablica:id/userNameInput")
    public WebElement input_namech;
    @FindBy(how = How.ID, using = "pl.tablica:id/saveButton")
    public WebElement btn_savech;
    @FindBy(how = How.ID, using = "pl.tablica:id/moreInfoIcon")
    public WebElement btn_minfo;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'ogłoszenie')]")
    public WebElement btn_addAd;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'przesyłki')]")
    public WebElement btn_packages;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kupujesz')]")
    public WebElement btn_buying;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Aktualne')]")
    public WebElement btn_actMesseg;
    @FindBy(how = How.ID, using = "pl.tablica:id/filterButton")
    public WebElement btn_messFiltr;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kupuję')]")
    public WebElement btn_buying2;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Portfel')]")
    public WebElement btn_pocket;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Historia')]")
    public WebElement btn_payHistory;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Tryb')]")
    public WebElement btn_darkTheme;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Ciemny')]")
    public WebElement btn_darkThemeConfirm;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'O aplikacji')]")
    public WebElement btn_appInfo;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wyloguj')]")
    public WebElement btn_logOut;
    @FindBy(how = How.ID, using = "pl.tablica:id/confirmButton")
    public WebElement btn_logOutConfirm;

    MethodHelper methodHelper = new MethodHelper();

    public void accNameChange(String namech) {
        System.out.println("Wchodzę w ustawienia konta...");
        btn_account.click();
        methodHelper.waitTime(2);
        System.out.println("Edytuję imię dla konta...");
        btn_edit.click();
        input_namech.sendKeys(namech);
        System.out.println("Zatwierdzam zmiany...");
        btn_savech.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
    }

    public void accInfo() {
        System.out.println("Wchodzę w informację...");
        btn_minfo.click();
        methodHelper.scrollText("pkt", direction.UP, "up", 0.8, 1);
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
    }

    public void accAdAdd() {
        System.out.println("Wchodzę w dodanie ogłoszenia...");
        btn_addAd.click();
        methodHelper.waitTime(5);
        methodHelper.pressAndroidBackBtn();
    }

    public void accPackages() {
        System.out.println("Wchodzę w moje przesyłki...");
        btn_packages.click();
        methodHelper.waitTime(2);
        btn_buying.click();
        methodHelper.pressAndroidBackBtn();
    }

    public void accMesseges() {
        System.out.println("Wchodzę w aktualne wiadomości...");
        btn_actMesseg.click();
        methodHelper.waitTime(2);
        btn_messFiltr.click();
        methodHelper.waitTime(2);
        btn_buying2.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
    }

    public void accWallet() {
        System.out.println("Wchodzę w portfel...");
        methodHelper.scrollText("Wyloguj", direction.UP, "up", 0.8, 1);
        methodHelper.waitTime(2);
        btn_pocket.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
        System.out.println("Wchodzę w Historię płatności...");
        methodHelper.waitTime(2);
        btn_payHistory.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
    }

    public void accSettings() {
        System.out.println("Wchodzę w ustawienia...");
        methodHelper.waitTime(5);
        methodHelper.tapCoordinates(1302, 1435);
        btn_darkTheme.click();
        System.out.println("Wybieram ciemny motyw aplikacji...");
        btn_darkThemeConfirm.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
        System.out.println("Wchodzę w info o aplikacji olx...");
        btn_appInfo.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
    }

    public void accLogOut() {
        System.out.println("Klikam wylogowanie...");
        btn_logOut.click();
        btn_logOutConfirm.click();
    }

}
