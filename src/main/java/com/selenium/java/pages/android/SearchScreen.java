package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchScreen extends BaseTest {
    @FindBy(how = How.ID, using = "pl.tablica:id/searchInput")
    public WebElement input_search;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'w Rowery górskie')]")
    public WebElement btn_bicycles;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'w Gry')]")
    public WebElement btn_games;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'w Baseny')]")
    public WebElement btn_pools;
    @FindBy(how = How.ID, using = "pl.tablica:id/sortButton")
    public WebElement btn_sort;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Najdroższe')]")
    public WebElement btn_sortByExpensive;
    @FindBy(how = How.ID, using = "pl.tablica:id/adViewTypeButton")
    public WebElement btn_viewType;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Lista')]")
    public WebElement btn_viewTypeList;
    @FindBy(how = How.ID, using = "pl.tablica:id/action_favorite")
    public WebElement btn_toFav;
    @FindBy(how = How.ID, using = "pl.tablica:id/linkUserAds")
    public WebElement btn_seller;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Obserwuj')]")
    public WebElement btn_observeSeller;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Obserwujesz')]")
    public WebElement btn_observed;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnReport")
    public WebElement btn_report;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnPhone")
    public WebElement btn_callSms;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnClose")
    public WebElement btn_callSmsBack;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnChat")
    public WebElement btn_chat;
    @FindBy(how = How.ID, using = "pl.tablica:id/chatFormBackButton")
    public WebElement btn_chatBack;
    @FindBy(how = How.ID, using = "pl.tablica:id/action_remove")
    public WebElement btn_removeAllObserve;
    @FindBy(how = How.ID, using = "pl.tablica:id/confirmButton")
    public WebElement btn_removeAllObserveConfirm;

    MethodHelper methodHelper = new MethodHelper();

    public void searchBicycle(String search) {
        System.out.println("Wpisuję dane wyszukiwania...");
        input_search.sendKeys(search);
        input_search.click();
        System.out.println("Szukam ogłoszeń...");
        btn_bicycles.click();
    }

    public void searchItems(String search) {
        System.out.println("Wpisuję dane wyszukiwania...");
        input_search.sendKeys(search);
        input_search.click();
        System.out.println("Szukam ogłoszeń...");
    }

    public void searchDifferentItems(String thing) {
        switch (thing) {
            case "bicycles":
                btn_bicycles.click();
                break;
            case "games":
                btn_games.click();
                break;
            case "pools":
                btn_pools.click();
                break;
        }
    }

    public void searchTap() {
        methodHelper.waitTime(5);
        methodHelper.tapCoordinates(1210, 756);

    }

    public void searchAddToFav() {
        System.out.println("Sortuję ogłoszenia od najdroższych...");
        btn_sort.click();
        btn_sortByExpensive.click();
        System.out.println("Wybieram pokazywanie ogłoszeń w formie listy...");
        btn_viewType.click();
        btn_viewTypeList.click();
        System.out.println("Wybieram konkretne ogłoszenie...");
        swipeInDirection(direction.UP, "up", 0.8);
        methodHelper.tapCoordinates(758, 1625);
        methodHelper.waitTime(5);
        methodHelper.tapCoordinates(1258, 729);
        methodHelper.waitTime(3);
        methodHelper.tapCoordinates(729, 616);
        methodHelper.waitTime(5);
        System.out.println("Swipuje zdjęcia w ogłoszeniu...");
        swipeInDirection(direction.RIGHT, "right", 0.8); //jak zrobic zeby kilka razy bez iteracji
        swipeInDirection(direction.RIGHT, "right", 0.8);
        swipeInDirection(direction.RIGHT, "right", 0.8);
        methodHelper.waitTime(3);
        System.out.println("Dodaję wybrane ogłoszenie do ulubionych");
        btn_toFav.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
        methodHelper.waitTime(2);
        swipeInDirection(direction.UP, "UP", 0.6);
        System.out.println("Wchodzę w ogłoszenia sprzedającego...");
        btn_seller.click();
        methodHelper.waitTime(2);
        System.out.println("Zaznaczam obserwowanie sprzedającego...");
        btn_observeSeller.click();
        methodHelper.pressAndroidBackBtn();
        methodHelper.waitTime(2);
        methodHelper.scrollId("pl.tablica:id/btnReport", direction.UP, "up", 0.8, 1);
        System.out.println("Zgłaszam ogłoszenie...");
        btn_report.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
        System.out.println("Wchodzę w menu dzwonienia...");
        btn_callSms.click();
        methodHelper.waitTime(2);
        btn_callSmsBack.click();
        methodHelper.waitTime(2);
        System.out.println("Wchodzę w menu chatu...");
        btn_chat.click();
        methodHelper.waitTime(2);
        btn_chatBack.click();
        methodHelper.waitTime(3);
        methodHelper.pressAndroidBackBtn();
        System.out.println("Przechodzę do moich obserwowanych ogłoszeń...");
        methodHelper.waitTime(2);
        btn_observed.click();
        methodHelper.waitTime(3);
        System.out.println("Usuwam wszystkie obserwowane ogłoszenia...");
        btn_removeAllObserve.click();
        methodHelper.waitTime(3);
        btn_removeAllObserveConfirm.click();
        methodHelper.pressAndroidBackBtn();
    }
}