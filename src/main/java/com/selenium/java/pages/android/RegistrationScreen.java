package com.selenium.java.pages.android;


import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class RegistrationScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/createAccountBtn")
    public WebElement btn_register;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;
    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;
    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"Show password\"]")
    public WebElement btn_showPassword;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_registerAccount;
    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement btn_checkbox;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnBrowse")
    public WebElement btn_goToHomeScreen;
    @FindBy(how = How.ID, using = "pl.tablica:id/actionButton")
    public WebElement btn_action;

    MethodHelper methodHelper = new MethodHelper();

    public void registrationToApp(String email, String password) {

        System.out.println("Uruchamiam aplikację...");
        btn_register.click();
        input_email.sendKeys(email);
        System.out.println("Wpisuję dane potrzebne do rejestracji...");
        input_password.sendKeys(password);
        System.out.println("Odkrywam hasło...");
        btn_showPassword.click();
        methodHelper.scrollId("pl.tablica:id/btnLogInNew", direction.UP, "up", 0.8, 1);
        System.out.println("Klikam zgodę na informacje handlowe...");
        btn_checkbox.click();
        System.out.println("Rejestruję użytkownika...");
        btn_registerAccount.click();
    }

    public void goToHomeScreen() {
        btn_goToHomeScreen.click();
        System.out.println("Zarejestrowano!");
        methodHelper.waitTime(2);
        methodHelper.scrollId("pl.tablica:id/actionButton", direction.UP, "up", 0.8, 1);
        btn_action.click();
        methodHelper.waitTime(2);
        methodHelper.tapCoordinates(1097, 2285);

    }
}