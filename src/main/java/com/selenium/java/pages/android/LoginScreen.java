package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class LoginScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/loginBtn")
    public WebElement btn_loginViaMail;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;
    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_login;
    @FindBy(how = How.ID, using = "pl.tablica:id/actionButton")
    public WebElement btn_action;
    @FindBy(how = How.ID, using = "pl.tablica:id/dismissBtn")
    public WebElement btn_dismiss;

    MethodHelper methodHelper = new MethodHelper();

    public void loginToApp(String email, String password) {

        System.out.println("Uruchamiam aplikację...");
        btn_loginViaMail.click();
        System.out.println("Wpisuje email i haslo...");
        methodHelper.waitTime(3);
        input_email.sendKeys(email);
        input_password.sendKeys(password);
        btn_login.click();
        System.out.println("Loguje...");
    }

    public void loginScrollClose() {

        System.out.println("Scrolluję aby zamknąć pop up...");
        methodHelper.waitTime(5);
        methodHelper.scrollId("pl.tablica:id/actionButton", direction.UP, "up", 0.8, 1);
        btn_action.click();
        System.out.println("Zalogowano");
        methodHelper.waitTime(2);
        methodHelper.tapCoordinates(1097, 2285);
        methodHelper.getScreenShot("Home.png");
    }

    public void loginAborted() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_dismiss.click();
    }
}


