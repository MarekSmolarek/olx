package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;


public class AddingAdScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj')]")
    public WebElement btn_dodaj;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj zdjęcia')]")//
    public WebElement btn_add_photo;
    @FindBy(how = How.CLASS_NAME, using = "android.widget.ImageView") //
    public List<WebElement> list_image;
    @FindBy(how = How.ID, using = "pl.tablica:id/action_done")//
    public WebElement btn_done;
    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement input_tytul;
    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_category;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatne')]")
    public WebElement btn_private;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Firma')]")
    public WebElement btn_corporate;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Używane')]")
    public WebElement btn_used;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Nowe')]")
    public WebElement btn_new;
    @FindBy(how = How.ID, using = "pl.tablica:id/priceTypeExchange")
    public WebElement btn_exchange;
    @FindBy(how = How.ID, using = "pl.tablica:id/priceTypeFree")
    public WebElement btn_free;
    @FindBy(how = How.ID, using = "pl.tablica:id/priceInput")
    public WebElement input_price;
    @FindBy(how = How.ID, using = "pl.tablica:id/priceArranged")
    public WebElement btn_nego;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Opisz')]")
    public WebElement input_opis;
    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_lok;
    @FindBy(how = How.ID, using = "pl.tablica:id/btn_my_location")
    public WebElement btn_mylok;
    @FindBy(how = How.ID, using = "android:id/button1")
    public WebElement btn_ok;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wybierz')]")
    public WebElement btn_choose;
    @FindBy(how = How.ID, using = "pl.tablica:id/backButton")
    public WebElement btn_back;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Numer telefonu')]")
    public List<WebElement> input_tel;
    @FindBy(how = How.ID, using = "pl.tablica:id/previewAdBtn")
    public WebElement btn_preview;

    MethodHelper methodHelper = new MethodHelper();

    public void adStartAdding() {

        System.out.println("Rozpoczynam dodawanie ogłoszenia...");
        btn_dodaj.click();
    }

    public void adAddingPhotos(int first, int second, int third, int fourth) {
        System.out.println("Dodaję zdjęcia...");
        methodHelper.waitTime(8);
        btn_add_photo.click();
        methodHelper.waitTime(5);
        for (int z = 0; z < list_image.size(); z++) {
            if (z == first) {
                list_image.get(first).click();
                list_image.get(second).click();
                list_image.get(third).click();
                list_image.get(fourth).click();
            }
        }
        btn_done.click();
    }

    public void adAddingTitle(String title) {
        System.out.println("Wpisuję tytuł...");
        input_tytul.sendKeys(title);
    }

    public void adCategoryChoose(String mainCat, String targetCat, String secondaryCat) {

        System.out.println("Wybieram kategorię...");
        btn_category.click();
        WebElement target_category = getDriver().findElement(By.xpath("//*[contains(@text, '" + targetCat + "')]"));
        WebElement main_category = getDriver().findElement(By.xpath("//*[contains(@text, '" + mainCat + "')]"));
        WebElement secondary_category = getDriver().findElement(By.xpath("//*[contains(@text, '" + secondaryCat + "')]"));

        try {

            target_category.click();

        } catch (Exception e) {
            try {
                methodHelper.scrollText(mainCat, direction.UP, "up", 0.8, 1);
                main_category.click();
                methodHelper.scrollText(targetCat, direction.UP, "up", 0.8, 1);

                target_category.click();
            } catch (Exception f) {

                methodHelper.scrollText(mainCat, direction.UP, "up", 0.8, 1);
                main_category.click();
                methodHelper.scrollText(secondaryCat, direction.UP, "up", 0.8, 1);
                secondary_category.click();
                methodHelper.scrollText(targetCat, direction.UP, "up", 0.8, 1);
                target_category.click();

            }
        }
        methodHelper.scrollText("Opisz", direction.UP, "up", 0.4, 1);

    }

    public void adPrivAndUsed(boolean priv, boolean used) {

        System.out.println("Zaznaczam atrybuty odnośnie rodzaju i stanu ogłoszenia...");
        if (priv) {
            btn_private.click();
        } else {
            btn_corporate.click();
        }
        if (used) {
            btn_used.click();
        } else {
            btn_new.click();
        }
    }

    public void adSellChangeGive(String dispose, String price) {
        System.out.println("Wybieram czy chce sprzedać, zamienić czy oddać za darmo...");
        switch (dispose) {
            case "sell":
                input_price.sendKeys(price);
                System.out.println("Zaznaczam cenę do negocjacji...");
                btn_nego.click();
                break;
            case "exchange":
                btn_exchange.click();
                break;
            case "give":
                btn_free.click();
                break;
        }
    }

    public void adAddingDescription(String descr) {
        System.out.println("Wpisuję opis...");
        input_opis.sendKeys(descr);
    }

    public void adSettingLocalization() {
        methodHelper.scrollText("Numer", direction.UP, "up", 0.7, 1);
        System.out.println("Wchodzę w lokalizację...");
        btn_lok.click();
        btn_mylok.click();
        try {
            btn_ok.click();
            btn_mylok.click();
            btn_back.click();
        } catch (Exception e) {
            btn_choose.click();
        }
    }

    public void adAddingPhone(String number) {
        System.out.println("Wpisuję nr telefonu...");
        methodHelper.scrollText("Zobacz", direction.UP, "up", 0.8, 1);
        for (int i = 0; i < input_tel.size(); i++) {

            if (i == 1) {
                input_tel.get(1).sendKeys(number);
            }
        }
    }

    public void adEndAdding() {
        System.out.println("Klikam zobacz przed dodaniem...");
        btn_preview.click();
        System.out.println("Ogłoszenie dodane :)");
    }
}
