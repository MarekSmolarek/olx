package com.selenium.java.pages.android;


import com.selenium.java.base.BaseTest;
import com.selenium.java.help.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;


public class FiltrScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Filtruj')]")
    public WebElement btn_filtr;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wybrane dla Ciebie')]")
    public WebElement btn_choosenToU;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Najtańsze')]")
    public WebElement btn_cheapest;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Najdroższe')]")
    public WebElement btn_mExpensive;
    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement btn_searchindescr;
    @FindBy(how = How.CLASS_NAME, using = "android.widget.ImageButton") //
    public List<WebElement> list_plusminus;
    @FindBy(how = How.ID, using = "pl.tablica:id/fromField")
    public WebElement input_minprice;
    @FindBy(how = How.ID, using = "pl.tablica:id/toField")
    public WebElement input_maxprice;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zamienię')]")
    public WebElement btn_exchange;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Za darmo')]")
    public WebElement btn_free;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatne')]")
    public WebElement btn_private;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Firma')]")
    public WebElement btn_corporate;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Używane')]")
    public WebElement btn_used;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Nowe')]")
    public WebElement btn_new;
    @FindBy(how = How.ID, using = "pl.tablica:id/view_type_gallery")
    public WebElement btn_viewGrid;
    @FindBy(how = How.ID, using = "pl.tablica:id/btnSubmit")
    public WebElement btn_submit;

    MethodHelper methodHelper = new MethodHelper();

    public void filtrFiltringItemsFirst(String sort) {
        System.out.println("Filtruję ogłoszenia...");
        btn_filtr.click();
        System.out.println("Zaznaczam sortowanie ...");

        switch (sort) {
            case "choosenToU":
                btn_choosenToU.click();
                break;
            case "cheapest":
                btn_cheapest.click();
                break;
            case "mExpensive":
                btn_mExpensive.click();
                break;
        }

        System.out.println("Klikam szukanie w opisach...");
        btn_searchindescr.click();
        methodHelper.scrollText("Stan", direction.UP, "up", 0.5, 1);
    }

    public void filtrMinMaxDistance() {
        System.out.println("Wyznaczam maksymalną odległość szukania ogłoszeń...");
        methodHelper.waitTime(20);
        for (int p = 0; p < list_plusminus.size(); p++) {
            if (p == 4) {
                for (int i = 0; i < 8; i++) {
                    list_plusminus.get(4).click();
                }
                for (int i = 0; i < 1; i++) {
                    list_plusminus.get(3).click();
                }
            }
        }
    }

    public void filtrSellChangeGive(String dispose, String minprice, String maxprice) {
        System.out.println("Wybieram czy chce sprzedać, zamienić czy oddać za darmo...");
        switch (dispose) {
            case "sell":
                input_minprice.sendKeys(minprice);
                input_maxprice.sendKeys(maxprice);
                break;
            case "exchange":
                btn_exchange.click();
                break;
            case "give":
                btn_free.click();
                break;
        }
        methodHelper.scrollText("MOZAIKA", direction.UP, "up", 0.5, 1);
    }

    public void filtrPrivAndUsed(boolean used,boolean priv) {

        System.out.println("Zaznaczam atrybuty odnośnie rodzaju i stanu ogłoszenia...");

        if (used) {
            btn_used.click();
        } else {
            btn_new.click();
        }
        if (priv) {
            btn_private.click();
        } else {
            btn_corporate.click();
        }
    }

    public void filtrFiltringItemsLast() {
        System.out.println("Zaznaczam wyświetlanie jako galeria...");
        btn_viewGrid.click();
        System.out.println("Zatwierdzam zmiany...");
        btn_submit.click();
        System.out.println("Filtracja zakończona pomyślnie :)");

    }
}















//package com.selenium.java.pages;
//
//
//import com.selenium.java.base.BaseTest;
//import com.selenium.java.help.MethodHelper;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.How;
//
//import java.util.List;
//
//
//public class FiltrScreen extends BaseTest {
//
//    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Filtruj')]")
//    public WebElement btn_filtr;
//    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Najtańsze')]")
//    public WebElement btn_najt;
//    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
//    public WebElement btn_searchindescr;
//    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wybierz')]")
//    public WebElement btn_lok;
//    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kujawsko-pomorskie')]")
//    public WebElement btn_lokkuj;
//    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Bydgoszcz')]")
//    public WebElement btn_lokbdg;
//    @FindBy(how = How.CLASS_NAME, using = "android.widget.ImageButton") //
//    public List<WebElement> list_plusminus;
//    @FindBy(how = How.ID, using = "pl.tablica:id/fromField")
//    public WebElement input_minprice;
//    @FindBy(how = How.ID, using = "pl.tablica:id/toField")
//    public WebElement input_maxprice;
//    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Używane')]")
//    public WebElement btn_used;
//    //   @FindBy(how = How.ID, using = "android:id/text1")
//    //   public WebElement btn_change;
//    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatne')]")
//    public WebElement btn_private;
//    @FindBy(how = How.ID, using = "pl.tablica:id/view_type_gallery")
//    public WebElement btn_viewGrid;
//    @FindBy(how = How.ID, using = "pl.tablica:id/btnSubmit")
//    public WebElement btn_submit;
//
//    MethodHelper methodHelper = new MethodHelper();
//
//    public void filtringItems(String minprice, String maxprice) {
//
//        btn_filtr.click();
//        System.out.println("Filtruję ogłoszenia...");
//        btn_najt.click();
//        System.out.println("Zaznaczam sortowanie od najtańszych...");
//        btn_searchindescr.click();
//        System.out.println("Klikam szukanie w opisach...");
//        btn_lok.click();
//        System.out.println("Wchodzę w lokalizację...");
//        btn_lokkuj.click();
//        System.out.println("Wybieram województwo i miasto...");
//        btn_lokbdg.click();
//        methodHelper.scrollText("Stan", direction.UP, "up", 0.5, 1);
//        System.out.println("Wyznaczam maksymalną odległość szukania ogłoszeń...");
//        methodHelper.waitTime(15);
//        for (int p = 0; p < list_plusminus.size(); p++) {
//            if (p == 4) {
//                for (int i = 0; i < 8; i++) {
//                    list_plusminus.get(4).click();
//                }
//                for (int i = 0; i < 1; i++) {
//                    list_plusminus.get(3).click();
//                }
//            }
//        }
//
//        System.out.println("Ustalam widełki cenowe...");
//        input_minprice.sendKeys(minprice);
//        input_maxprice.sendKeys(maxprice);
//        System.out.println("Zaznaczam ogłoszenia używane...");
//        btn_used.click();
//        //  System.out.println("Zaznaczam atrybut 'Zamienię'...");
//        //  btn_change.click();
//        methodHelper.scrollText("MOZAIKA", direction.UP, "up", 0.5, 1);
//        System.out.println("Zaznaczam ogłoszenia tylko prywatne...");
//        btn_private.click();
//        System.out.println("Zaznaczam wyświetlanie jako galeria...");
//        btn_viewGrid.click();
//        System.out.println("Zatwierdzam zmiany...");
//        btn_submit.click();
//        System.out.println("Filtracja zakończona pomyślnie :)");
//
//    }
//}
